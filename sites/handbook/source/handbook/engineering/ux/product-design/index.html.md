---
layout: handbook-page-toc
title: "Product Design"
description: "We support the business of GitLab by becoming experts in our stage group, educating ourselves about the entire product, and staying engaged with user and business goals"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}

## Product Design at GitLab

We support the business of GitLab by becoming experts in our stage group, educating ourselves about the entire product, and staying engaged with user and business goals. We partner closely with our stable counterparts in Product Management and Development. 


## Team Structure

Each Product Designer is assigned to an area of our product, called Stage Groups. They learn everything they can about users and their workflows to design solutions for real customer problems. 

Information about product categories and links to team members and direction pages can be found [here](/handbook/product/categories/).

Some UX teams have documented detailed information about their ways of working and stage groups, and these can be found here: 
* [Ops](https://about.gitlab.com/direction/ops/) (@gitlab-com/gitlab-ux/ops-ux)
    * [CI/CD](/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/) (@gitlab-com/gitlab-ux/cicd-ux)
        * [Verify UX](/handbook/engineering/ux/stage-group-ux-strategy/verify/)
        * [Package UX](/handbook/engineering/ux/stage-group-ux-strategy/package/)
        * [Release UX](/handbook/engineering/ux/stage-group-ux-strategy/release/)
    * Configure and Monitor (@gitlab-com/gitlab-ux/configure-monitor-ux)
        * [Configure](https://about.gitlab.com/direction/configure/)
        * [Monitor](https://about.gitlab.com/direction/monitor/)
* [Enablement](/handbook/engineering/ux/stage-group-ux-strategy/enablement/) (@gitlab-com/gitlab-ux/enablement-ux)
* [Sec](https://about.gitlab.com/direction/security/)
    * [Secure and Protect](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/sec/) (@gitlab-com/gitlab-ux/secure-protect-ux)

## Product Design Workflow

Product Designers follow the guidance outlined in the [Product Development flow](/handbook/product-development-flow/) while working on stage group work with our stable counterparts. 

For specific details:
* [Planning and managing capacity](/handbook/engineering/ux/product-designer/#planning-and-managing-capacity)
* [Prioritization](/handbook/engineering/ux/product-designer/#priority-for-UX-issues)
* [Working on Issues](/handbook/engineering/ux/product-designer/#working-on-issues) 
* [Design Process](/handbook/engineering/ux/product-designer/#product-design-process)
* [Partnering with UX Resesarch](/handbook/engineering/ux/product-designer/#product-design-process)
* [Partnering with Technical Writers](/handbook/engineering/ux/product-designer/#partnering-with-technical-writers)
* [Contributing to Pajamas](https://design.gitlab.com/get-started/contribute)

Are you a new GitLab Product Designer? If so, welcome! Make sure you see the [Product Designer Workflow](/handbook/engineering/ux/product-designer/) handbook page that will help you get started.

## Learn about UX and see our work

* [YouTube Playlist for UX Showcases](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz) 
* [UX Learning and Development page](/handbook/engineering/ux/learning-and-development)

## Design Principles

Our [design principles](https://design.gitlab.com/get-started/principles) can be found with the Pajamas Design System. 


